/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup USB
 * @{
 *
 * @brief Defines the standard APIs of the USB function.
 *
 * This module declares the custom data types and functions used to obtain descriptors, interface objects, and request objects, and to submit requests.
 *
 * @since 3.0
 * @version 1.0
 */

/**
 * @file usbd_type.h
 *
 * @brief Declares the custom data types used in the APIs of the USB driver module.
 *
 * Such data types include command words and data directions.
 *
 * @since 3.0
 * @version 1.0
 */

#ifndef USBD_TYPE_H
#define USBD_TYPE_H

#include <stdint.h>
#include <stdlib.h>
#include <string>
#include <vector>

/** Maximum number of USB interfaces */
#define USB_MAX_INTERFACES 32

/** Bit mask for extracting the USB Endpoint direction from the address */
static const int32_t USB_ENDPOINT_DIR_MASK = 0x80;

/** Data direction of the USB Endpoint from the device to the host */
static const int32_t USB_ENDPOINT_DIR_IN = 0x80;

/** Data direction of the USB Endpoint from the host to the device */
static const int32_t USB_ENDPOINT_DIR_OUT = 0;

/** Request callback */
typedef void (*UsbdRequestCallback)(uint8_t *requestArg);

/**
 * @brief Defines the command word for bulk callback.
 *
 */
enum UsbdBulkCbCmd {
    /** Bulk callback reading */
    CMD_USBD_BULK_CALLBACK_READ,
    /** Bulk callback writing */
    CMD_USBD_BULK_CALLBACK_WRITE,
};

/**
 * @brief Defines hot swap events of the USB host and device.
 *
 */
enum UsbdDeviceAction {
    /** Device connected to the host */
    ACT_DEVUP = 0,
    /** Device removed from the host */
    ACT_DEVDOWN,
    /** Device connected */
    ACT_UPDEVICE,
    /** Device disconnected */
    ACT_DOWNDEVICE,
};

namespace OHOS {
namespace USB {
/**
 * @brief Defines a USB device.
 *
 */
struct UsbDev {
    /** USB bus ID */
    uint8_t busNum;
    /** USB device address */
    uint8_t devAddr;
};

/**
 * @brief Defines a USB pipe.
 *
 */
struct UsbPipe {
    /** USB interface ID */
    uint8_t interfaceId;
    /** USB endpoint ID */
    uint8_t endpointId;
};

/**
 * @brief Performs USB control transfer.
 *
 */
struct UsbCtrlTransfer {
    /** Request type */
    int32_t requestType;
    /** Request command word */
    int32_t requestCmd;
    /** Request value */
    int32_t value;
    /** Index */
    int32_t index;
    /** Timeout interval */
    int32_t timeout;
};

/**
 * @brief Defines the USB device information.
 *
 */
struct USBDeviceInfo {
    /** USB device status */
    int32_t status;
    /** USB bus ID */
    int32_t busNum;
    /** USB device ID */
    int32_t devNum;
};
} /** namespace USB */
} /** namespace OHOS */

#endif /** USBD_TYPE_H */
