/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Vibrator
 * @{
 *
 * @brief 马达驱动对马达服务提供通用的接口能力。
 *
 * 服务获取驱动对象或者代理后，马达服务启动或停止振动。
 * 通过驱动程序对象或代理提供使用功能。
 *
 * @since 2.2
 */

/**
 * @file vibrator_if.h
 *
 * @brief 定义马达数据结构，包括马达模式和效果振动。
 *
 * @since 2.2
 * @version 1.0
 */

#ifndef VIBRATOR_IF_H
#define VIBRATOR_IF_H

#include <stdint.h>
#include "vibrator_type.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief 提供vibrator设备基本控制操作接口。
 *
 * 操作包括马达模式和效果振动、停止马达振动。
 */

struct VibratorInterface {
    /**
     * @brief 控制马达以执行给定持续时间的一次性振动。
     *
     * 单次振动与周期振动相互排斥。在执行一次性振动之前，需退出周期性振动。
     *
     * @param duration 指示一次性振动的持续时间，以毫秒为单位。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    int32_t (*StartOnce)([in] uint32_t duration);

    /**
     * @brief 控制马达以预置效果执行周期性振动。
     *
     * 单次振动与周期振动相互排斥。在执行一次性振动之前，需退出周期性振动。
     *
     * @param effectType 指向指示预置效果类型的指针。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    int32_t (*Start)([in] const char *effectType);

    /**
     * @brief 停止马达振动。
     *
     * 马达启动前，必须在任何模式下停止振动。此功能用在振动过程之后。
     *
     * @param mode 指示振动模式，可以是一次性或周期性的，详见{@link VibratorMode}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    int32_t (*Stop)([in] enum VibratorMode mode);

    /**
     * @brief 获取有关系统中支持设置振幅和频率的所有马达信息。
     *
     * @param vibratorInfo 表示指向马达信息的指针，详见{@link VibratorInfo}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.1
     */
    int32_t (*GetVibratorInfo)([out] struct VibratorInfo **vibratorInfo);
    /**
     * @brief 根据传入的振动效果启动马达。
     *
     * @param duration 表示马达振动的持续时间，以毫秒为单位。
     * @param intensity 表示振动周期内的马达振幅。
     * @param frequency 表示振动周期内的马达频率。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果不支持振动周期设置，则返回-1。
     * @return 如果不支持振幅设置，则返回-2。
     * @return 如果不支持频率设置，则返回-3。
     *
     * @since 3.2
     * @version 1.1
     */
    int32_t (*EnableVibratorModulation)(uint32_t duration, int32_t intensity, int32_t frequency);
};

/**
 * @brief 创建一个VibratorInterface实例。
 *
 * 获的马达接口实例可用于控制马达按照配置进行振动。
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 2.2
 * @version 1.0
 */
const struct VibratorInterface *NewVibratorInterfaceInstance(void);

/**
 * @brief 释放VibratorInterface实例以及相关资源。
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 2.2
 * @version 1.0
 */
int32_t FreeVibratorInterfaceInstance(void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* VIBRATOR_IF_H */
/** @} */
